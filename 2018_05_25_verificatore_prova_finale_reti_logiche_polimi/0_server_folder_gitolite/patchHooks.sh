############################################################################
 #   Copyright (C) 2018 Davide Zoni                                       # 
 #                                                                        # 
 #   This program is free software; you can redistribute it and/or modify # 
 #   it under the terms of the GNU General Public License as published by # 
 #   the Free Software Foundation; either version 2 of the License, or    # 
 #   (at your option) any later version.                                  # 
 #                                                                        # 
 #   This program is distributed in the hope that it will be useful,      # 
 #   but WITHOUT ANY WARRANTY; without even the implied warranty of       # 
 #   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
 #   GNU General Public License for more details.                         # 
 #                                                                        # 
 #   You should have received a copy of the GNU General Public License    # 
 #   along with this program; if not, see <http://www.gnu.org/licenses/>  # 
############################################################################
 

# Has to be executed server side after the creation of all the 
# apply the post-receive hooks to all the repositories but two:
## the gitolite-admin.git and testing.git

#!/bin/sh
for i in repositories/*; do
	#echo $i
	
	if [[ -d $i  && $i != "repositories/gitolite-admin.git" && $i != "repositories/testing.git" ]]; then
		#apply post-receive hook script
		echo $i"/hooks/post-receive     (applying here)"
		cp post-receive.template ${i}/hooks/post-receive	
	fi
done
