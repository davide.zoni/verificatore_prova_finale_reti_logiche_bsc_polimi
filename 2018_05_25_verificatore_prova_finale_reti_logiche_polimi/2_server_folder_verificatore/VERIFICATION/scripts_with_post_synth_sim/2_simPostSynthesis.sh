############################################################################
 #   Copyright (C) 2018 Davide Zoni                                       # 
 #                                                                        # 
 #   This program is free software; you can redistribute it and/or modify # 
 #   it under the terms of the GNU General Public License as published by # 
 #   the Free Software Foundation; either version 2 of the License, or    # 
 #   (at your option) any later version.                                  # 
 #                                                                        # 
 #   This program is distributed in the hope that it will be useful,      # 
 #   but WITHOUT ANY WARRANTY; without even the implied warranty of       # 
 #   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
 #   GNU General Public License for more details.                         # 
 #                                                                        # 
 #   You should have received a copy of the GNU General Public License    # 
 #   along with this program; if not, see <http://www.gnu.org/licenses/>  # 
############################################################################

#!/bin/bash
# copy testbench portions for post synthesis simulation


TESTBENCH_FILE=$1


TESTBENCH_NAME='project_tb'
SIMULATION_TIMEOUT_SECONDS=300

TARGET_TOP_NAME='project_reti_logiche'


rm *.prj *.log *.jou -rf xsim.dir;


# 1) Generate project files for vivado post-synth simulation

# check synthezised file before running post synth simulation
if [[ -f ${TARGET_TOP_NAME}_synth.v ]]; then
	echo "verilog work ${TARGET_TOP_NAME}_synth.v" >> ./target_sim_postsynth_verilog.prj;
	# LOAD THE TESTBENCH
	echo "vhdl work $1"  >> ./target_sim_postsynth_vhdl.prj
fi


# 2) Generate the simulation and simulate
if [[ -f ./target_sim_postsynth_vhdl.prj ]]; then
	xvhdl -m64 --relax -prj ./target_sim_postsynth_vhdl.prj -log xvhdl_postsynth.log
fi

if [[ -f ./target_sim_postsynth_verilog.prj ]]; then 
	xvlog -m64 --relax -prj ./target_sim_postsynth_verilog.prj -log xvlog_postsynth.log
fi

xelab -sv --relax --mt 8 -L xil_defaultlib glbl -L unimacro_ver -L secureip -L unisims_ver --snapshot target_postsynth ${TESTBENCH_NAME} -log elaborate_postsynth.log --debug all --timescale 1ns/100ps

# the simulation has a timeout of TIMEOUT seconds. Check if timeout elapsed using $@ == 124
timeout ${SIMULATION_TIMEOUT_SECONDS} xsim target_postsynth -t ./run_sim.tcl -log xsim_postsynth.log

echo $?

#TODO return timeout

#TODO hide sensible information (by sed them :) )

if [[ -f xsim_postsynth.log ]]; then 
	sed -i "s/home\/verificatore/BLIND_PATH/g" xsim_postsynth.log
fi

if [[ -f elaborate_postsynth.log ]]; then 
	sed -i "s/home\/verificatore/BLIND_PATH/g" elaborate_postsynth.log
fi

if [[ -f xvhdl_postsynth.log ]]; then 
	sed -i "s/home\/verificatore/BLIND_PATH/g" xvhdl_postsynth.log
fi

if [[ -f xvlog_postsynth.log ]]; then 
	sed -i "s/home\/verificatore/BLIND_PATH/g" xvlog_postsynth.log
fi



