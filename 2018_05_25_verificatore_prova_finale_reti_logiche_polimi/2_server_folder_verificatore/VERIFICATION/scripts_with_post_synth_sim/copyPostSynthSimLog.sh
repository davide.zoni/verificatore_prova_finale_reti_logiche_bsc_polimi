############################################################################
 #   Copyright (C) 2018 Davide Zoni                                       # 
 #                                                                        # 
 #   This program is free software; you can redistribute it and/or modify # 
 #   it under the terms of the GNU General Public License as published by # 
 #   the Free Software Foundation; either version 2 of the License, or    # 
 #   (at your option) any later version.                                  # 
 #                                                                        # 
 #   This program is distributed in the hope that it will be useful,      # 
 #   but WITHOUT ANY WARRANTY; without even the implied warranty of       # 
 #   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
 #   GNU General Public License for more details.                         # 
 #                                                                        # 
 #   You should have received a copy of the GNU General Public License    # 
 #   along with this program; if not, see <http://www.gnu.org/licenses/>  # 
############################################################################

#!/bin/bash

DEST_FOLDER=$1
# create folder for results
mkdir ${DEST_FOLDER}

# add POST_SYNTH_SIM logs
if [[ -f xvlog_postsynth.log ]]; then
	sed -i "s/home\/verificatore/BLIND_PATH/g" xvlog_postsynth.log
	sed -i "s/home\/dzoni/BLIND_PATH/g" xvlog_postsynth.log
	cp xvlog_postsynth.log ${DEST_FOLDER}
fi

if [[ -f xvhdl_postsynth.log ]]; then
	sed -i "s/home\/verificatore/BLIND_PATH/g" xvhdl_postsynth.log
	sed -i "s/home\/dzoni/BLIND_PATH/g" xvhdl_postsynth.log
	cp xvhdl_postsynth.log ${DEST_FOLDER}
fi
 
if [[ -f elaborate_postsynth.log ]]; then
	sed -i "s/home\/verificatore/BLIND_PATH/g" elaborate_postsynth.log
	sed -i "s/home\/dzoni/BLIND_PATH/g" elaborate_postsynth.log
	cp elaborate_postsynth.log ${DEST_FOLDER}
fi

if [[ -f xsim_postsynth.log ]]; then
	sed -i "s/home\/verificatore/BLIND_PATH/g" xsim_postsynth.log
	sed -i "s/home\/dzoni/BLIND_PATH/g" xsim_postsynth.log
	cp xsim_postsynth.log ${DEST_FOLDER}
fi 
