############################################################################
 #   Copyright (C) 2018 Davide Zoni                                       # 
 #                                                                        # 
 #   This program is free software; you can redistribute it and/or modify # 
 #   it under the terms of the GNU General Public License as published by # 
 #   the Free Software Foundation; either version 2 of the License, or    # 
 #   (at your option) any later version.                                  # 
 #                                                                        # 
 #   This program is distributed in the hope that it will be useful,      # 
 #   but WITHOUT ANY WARRANTY; without even the implied warranty of       # 
 #   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
 #   GNU General Public License for more details.                         # 
 #                                                                        # 
 #   You should have received a copy of the GNU General Public License    # 
 #   along with this program; if not, see <http://www.gnu.org/licenses/>  # 
############################################################################

#!/bin/bash

DEST_FOLDER=$1

# create folder for results
mkdir ${DEST_FOLDER}

# add PRE_SYNTH_SIM logs
if [[ -f xvlog.log ]]; then
	sed -i "s/home\/verificatore/BLIND_PATH/g" xvlog.log
	sed -i "s/home\/dzoni/BLIND_PATH/g" xvlog.log
	cp xvlog.log ${DEST_FOLDER}
fi

if [[ -f xvhdl.log ]]; then
	sed -i "s/home\/verificatore/BLIND_PATH/g" xvhdl.log
	sed -i "s/home\/dzoni/BLIND_PATH/g" xvhdl.log
	cp xvhdl.log ${DEST_FOLDER}
fi
 
if [[ -f elaborate.log ]]; then 
	sed -i "s/home\/verificatore/BLIND_PATH/g" elaborate.log
	sed -i "s/home\/dzoni/BLIND_PATH/g" elaborate.log
	cp elaborate.log ${DEST_FOLDER}
fi

if [[ -f xsim.log ]]; then	
	sed -i "s/home\/verificatore/BLIND_PATH/g" xsim.log 
	sed -i "s/home\/dzoni/BLIND_PATH/g" xsim.log 
	cp xsim.log ${DEST_FOLDER}
fi 
