############################################################################
 #   Copyright (C) 2018 Davide Zoni                                       # 
 #                                                                        # 
 #   This program is free software; you can redistribute it and/or modify # 
 #   it under the terms of the GNU General Public License as published by # 
 #   the Free Software Foundation; either version 2 of the License, or    # 
 #   (at your option) any later version.                                  # 
 #                                                                        # 
 #   This program is distributed in the hope that it will be useful,      # 
 #   but WITHOUT ANY WARRANTY; without even the implied warranty of       # 
 #   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
 #   GNU General Public License for more details.                         # 
 #                                                                        # 
 #   You should have received a copy of the GNU General Public License    # 
 #   along with this program; if not, see <http://www.gnu.org/licenses/>  # 
############################################################################

#!/bin/bash
# copy testbench portions for post synthesis simulation

PROJ_FOLDER=$1
HDL_SRC_FOLDER=$2

SYNTHESIS_TIMEOUT_SECONDS=300

TARGET_TOP_NAME=project_reti_logiche



rm target_synth.prj *.jou

# 1) Generate project files for vivado simulation
for i in `find ../${HDL_SRC_FOLDER}/${PROJ_FOLDER} -maxdepth 1 -type f`; do

    # simple check to correctly generate project files for vivado
    if [[ $i == *vhd || $i == *vhdl ]];
    then
        echo "read_vhdl " $i >> ./target_synth.prj;
    else
        if [[ $i == *sv ]];
        then
            echo "read_verilog -sv" $i >> ./target_synth.prj;
        else 
             if [[ $i == *v ]]; then
               echo "read_verilog " $i >> ./target_synth.prj;
             fi
        fi
    fi
done

echo "">> target_synth.prj;

echo "create_fileset -constrset userConstraints" >> target_synth.prj
echo "add_files -fileset userConstraints constr.xdc" >> target_synth.prj

echo "">> target_synth.prj;
#SYNTHESIS
echo "synth_design -top ${TARGET_TOP_NAME} -part xc7a200tfbg484-1 -constrset userConstraints -flatten_hierarchy none"  >> target_synth.prj
echo "write_verilog -force ./${TARGET_TOP_NAME}_synth.v -mode funcsim -write_all_overrides" >> target_synth.prj

#quit vivado
echo "exit">> target_synth.prj



# synthesize
timeout ${SYNTHESIS_TIMEOUT_SECONDS} vivado -mode batch -source target_synth.prj -log synth.log
# the simulation has a timeout of TIMEOUT seconds. Check if timeout elapsed using $@ == 124
echo $?





##TODO hide sensible information (by sed them :) )
#
#if [[ -f xsim.log ]]; then 
#	sed -i "s/home\/verificatore/BLIND_PATH/g" xsim.log
#fi
#
#if [[ -f elaborate.log ]]; then 
#	sed -i "s/home\/verificatore/BLIND_PATH/g" elaborate.log
#fi
#
#if [[ -f xvhdl.log ]]; then 
#	sed -i "s/home\/verificatore/BLIND_PATH/g" xvhdl.log
#fi
#
#if [[ -f xvlog.log ]]; then 
#	sed -i "s/home\/verificatore/BLIND_PATH/g" xvlog.log
#fi



