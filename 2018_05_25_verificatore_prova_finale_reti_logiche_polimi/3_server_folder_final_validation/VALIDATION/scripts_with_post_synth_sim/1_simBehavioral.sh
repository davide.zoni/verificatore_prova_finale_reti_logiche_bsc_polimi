############################################################################
 #   Copyright (C) 2018 Davide Zoni                                       # 
 #                                                                        # 
 #   This program is free software; you can redistribute it and/or modify # 
 #   it under the terms of the GNU General Public License as published by # 
 #   the Free Software Foundation; either version 2 of the License, or    # 
 #   (at your option) any later version.                                  # 
 #                                                                        # 
 #   This program is distributed in the hope that it will be useful,      # 
 #   but WITHOUT ANY WARRANTY; without even the implied warranty of       # 
 #   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
 #   GNU General Public License for more details.                         # 
 #                                                                        # 
 #   You should have received a copy of the GNU General Public License    # 
 #   along with this program; if not, see <http://www.gnu.org/licenses/>  # 
############################################################################

#!/bin/bash
# copy testbench portions for post synthesis simulation

PROJ_FOLDER=$1
HDL_SRC_FOLDER=$2
TESTBENCH_FILE=$3
TESTBENCH_NAME='project_tb'

SIMULATION_TIMEOUT_SECONDS=300



rm *.prj *.log *.jou -rf xsim.dir;
rm -fr xsim.dir

# 1) Generate project files for vivado simulation

for i in `find ../${HDL_SRC_FOLDER}/${PROJ_FOLDER} -maxdepth 1 -type f`; do

    # simple check to correctly generate project files for vivado
    if [[ $i == *vhd || $i == *vhdl ]];
    then
        echo "vhdl work" $i >> ./target_sim_vhdl.prj;
    else
        if [[ $i == *sv ]];
        then
            echo "sv work " $i >> ./target_sim_sv.prj;
        else
			if [[ $i == *v ]];
			then	
	            echo "verilog work" $i >> ./target_sim_verilog.prj;
			fi
        fi
    fi
done


# FIXME check to avoid being trolled by the student that submit its fake tb with the same name
# We exploit the compile order to be sure that our test bench will be the last to be executed.

# LOAD THE TESTBENCH
#echo "verilog work ../tb/tb.v"  >> ./target_sim_verilog.prj
#echo "vhdl work ../tb/testbench.vhd"  >> ./target_sim_vhdl.prj
echo "vhdl work ${TESTBENCH_FILE}"  >> ./target_sim_vhdl.prj




# 2) Generate the simulation and simulate
if [[ -f target_sim_vhdl.prj ]]; then
	xvhdl -m64 --relax -prj ./target_sim_vhdl.prj
fi

if [[ -f target_sim_sv.prj ]]; then 
	xvlog -sv -m64 --relax -prj ./target_sim_sv.prj
fi

if [[ -f target_sim_verilog.prj ]]; then 
	xvlog -m64 --relax -prj ./target_sim_verilog.prj
fi

xelab -sv --relax --mt 8 -L xil_defaultlib -L unimacro_ver -L secureip -L unisims_ver --snapshot target_behav ${TESTBENCH_NAME} -log elaborate.log --debug all --timescale 1ns/100ps

# the simulation has a timeout of TIMEOUT seconds. Check if timeout elapsed using $@ == 124
timeout ${SIMULATION_TIMEOUT_SECONDS} xsim target_behav -t ./run_sim.tcl

echo $?

#TODO return timeout

#TODO hide sensible information (by sed them :) )

if [[ -f xsim.log ]]; then 
	sed -i "s/home\/verificatore/BLIND_PATH/g" xsim.log
fi

if [[ -f elaborate.log ]]; then 
	sed -i "s/home\/verificatore/BLIND_PATH/g" elaborate.log
fi

if [[ -f xvhdl.log ]]; then 
	sed -i "s/home\/verificatore/BLIND_PATH/g" xvhdl.log
fi

if [[ -f xvlog.log ]]; then 
	sed -i "s/home\/verificatore/BLIND_PATH/g" xvlog.log
fi



