############################################################################
 #   Copyright (C) 2018 Davide Zoni                                       # 
 #                                                                        # 
 #   This program is free software; you can redistribute it and/or modify # 
 #   it under the terms of the GNU General Public License as published by # 
 #   the Free Software Foundation; either version 2 of the License, or    # 
 #   (at your option) any later version.                                  # 
 #                                                                        # 
 #   This program is distributed in the hope that it will be useful,      # 
 #   but WITHOUT ANY WARRANTY; without even the implied warranty of       # 
 #   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
 #   GNU General Public License for more details.                         # 
 #                                                                        # 
 #   You should have received a copy of the GNU General Public License    # 
 #   along with this program; if not, see <http://www.gnu.org/licenses/>  # 
############################################################################

#!/bin/bash

PROJ_FOLDER=$1
HDL_SRC_FOLDER=$2

SCRIPT_PWD=`pwd`

RESULTS_FOLD_NAME='RESULTS'

# create folder for results
mkdir ../${HDL_SRC_FOLDER}/${PROJ_FOLDER}/${RESULTS_FOLD_NAME}

#commit results to make them visible to the user
cd ../${HDL_SRC_FOLDER}/${PROJ_FOLDER}/
#set git identity
git config user.email "verificatore@polimi.it"
git config user.name "VERIFICATORE"
git config push.default simple
#add files and commit
git add ${RESULTS_FOLD_NAME}
git commit -m "VERIFICATION RESULTS"
git push

# move back to script folder
cd $SCRIPT_PWD 
