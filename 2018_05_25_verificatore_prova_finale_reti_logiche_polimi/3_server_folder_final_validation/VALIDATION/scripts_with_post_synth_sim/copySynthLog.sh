############################################################################
 #   Copyright (C) 2018 Davide Zoni                                       # 
 #                                                                        # 
 #   This program is free software; you can redistribute it and/or modify # 
 #   it under the terms of the GNU General Public License as published by # 
 #   the Free Software Foundation; either version 2 of the License, or    # 
 #   (at your option) any later version.                                  # 
 #                                                                        # 
 #   This program is distributed in the hope that it will be useful,      # 
 #   but WITHOUT ANY WARRANTY; without even the implied warranty of       # 
 #   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
 #   GNU General Public License for more details.                         # 
 #                                                                        # 
 #   You should have received a copy of the GNU General Public License    # 
 #   along with this program; if not, see <http://www.gnu.org/licenses/>  # 
############################################################################

#!/bin/bash

PROJ_FOLDER=$1
HDL_SRC_FOLDER=$2

SCRIPT_PWD=`pwd`

RESULTS_FOLD_NAME='RESULTS'

# create folder for results
mkdir ../${HDL_SRC_FOLDER}/${PROJ_FOLDER}/${RESULTS_FOLD_NAME}

# add SYNTHESIS log
if [[ -f synth.log ]]; then
	sed -i "s/home\/verificatore/BLIND_PATH/g" synth.log
	sed -i "s/home\/dzoni/BLIND_PATH/g" synth.log
	cp synth.log ../${HDL_SRC_FOLDER}/${PROJ_FOLDER}/${RESULTS_FOLD_NAME}
fi
