############################################################################
 #   Copyright (C) 2018 Luca Cremona, Davide Zoni                         # 
 #                                                                        # 
 #   This program is free software; you can redistribute it and/or modify # 
 #   it under the terms of the GNU General Public License as published by # 
 #   the Free Software Foundation; either version 2 of the License, or    # 
 #   (at your option) any later version.                                  # 
 #                                                                        # 
 #   This program is distributed in the hope that it will be useful,      # 
 #   but WITHOUT ANY WARRANTY; without even the implied warranty of       # 
 #   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
 #   GNU General Public License for more details.                         # 
 #                                                                        # 
 #   You should have received a copy of the GNU General Public License    # 
 #   along with this program; if not, see <http://www.gnu.org/licenses/>  # 
############################################################################

#!/bin/bash

# behav / post_synth
RES_FOLDER=$1
NUM_TESTS=$2
RES_FILENAME=$3

XSIM_FILE_NAME_B="xsim.log"
XSIM_FILE_NAME_P="xsim_postsynth.log"
SIM_TYPE_B="behav"
SIM_TYPE_P="post_synth"

SCRIPT_FOLDER=`pwd`


tmp=${RES_FOLDER#*/src/}
STUDENT_ID=${tmp%/RESULTS*}

cd $RES_FOLDER

a=0;
b=0;

echo "Analyzing ${SIM_TYPE_B} results (NUM_TESTS: ${NUM_TESTS}, result folder ${STUDENT_ID})"
for i in $(seq 1 $NUM_TESTS);do
		rGrep=`grep -in "test passed" test$i/${SIM_TYPE_B}/${XSIM_FILE_NAME_B}`
		if [ "$rGrep" ]; then 
			echo -e test${i} '\t' ok;
			a=$(($a+1))
		else
			echo -e test${i} '\t' failed; 
		fi
		rGrep=''
done

echo "Analyzing ${SIM_TYPE_P} results (NUM_TESTS: ${NUM_TESTS}, result folder ${STUDENT_ID})"
for i in $(seq 1 $NUM_TESTS);do
		rGrep=`grep -in "test passed" test$i/${SIM_TYPE_P}/${XSIM_FILE_NAME_P}`
		if [ "$rGrep" ]; then 
			echo -e test${i} '\t' ok;
			b=$(($b+1))
		else
			echo -e test${i} '\t' failed; 
		fi
		rGrep=''
done

echo ""
echo "#######################################################"
echo "#######################################################"
echo "Summary of SUCCESSFUL tests behav:"  $a " out of " $NUM_TESTS
echo "Summary of SUCCESSFUL tests post_synth:"  $b " out of " $NUM_TESTS
echo "#######################################################"
echo "#######################################################"
echo ""

cd ${SCRIPT_FOLDER}

echo "Writing to $RES_FILENAME"
echo -e "$STUDENT_ID \t $NUM_TESTS \t $a \t $b" >> $RES_FILENAME


