############################################################################
 #   Copyright (C) 2018 Luca Cremona and Davide Zoni                      # 
 #                                                                        # 
 #   This program is free software; you can redistribute it and/or modify # 
 #   it under the terms of the GNU General Public License as published by # 
 #   the Free Software Foundation; either version 2 of the License, or    # 
 #   (at your option) any later version.                                  # 
 #                                                                        # 
 #   This program is distributed in the hope that it will be useful,      # 
 #   but WITHOUT ANY WARRANTY; without even the implied warranty of       # 
 #   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
 #   GNU General Public License for more details.                         # 
 #                                                                        # 
 #   You should have received a copy of the GNU General Public License    # 
 #   along with this program; if not, see <http://www.gnu.org/licenses/>  # 
############################################################################

#!/bin/bash

RES_FILENAME="prj_rl_results.csv"
NUM_TEST=30
SRC_PATH="/home/verifica-prj-rl/VERIFICATORE_FOLDER_WITH_SIM_POSTSYNTH/src/"

echo "STUDENT_ID \t NUM_TEST \t BEHAV_PASS P_S_PASS" >> $RES_FILENAME

for i in `ls -v $SRC_PATH`; do
	FULL_PATH="$SRC_PATH/$i/RESULTS/"
	./genResStatistics.sh $FULL_PATH $NUM_TEST $RES_FILENAME
done
