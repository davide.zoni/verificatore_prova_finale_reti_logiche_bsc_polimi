############################################################################
 #   Copyright (C) 2018 Davide Zoni                                       # 
 #                                                                        # 
 #   This program is free software; you can redistribute it and/or modify # 
 #   it under the terms of the GNU General Public License as published by # 
 #   the Free Software Foundation; either version 2 of the License, or    # 
 #   (at your option) any later version.                                  # 
 #                                                                        # 
 #   This program is distributed in the hope that it will be useful,      # 
 #   but WITHOUT ANY WARRANTY; without even the implied warranty of       # 
 #   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
 #   GNU General Public License for more details.                         # 
 #                                                                        # 
 #   You should have received a copy of the GNU General Public License    # 
 #   along with this program; if not, see <http://www.gnu.org/licenses/>  # 
############################################################################
 

#!/bin/bash
NAME_FILE="studenti_real.txt"
KEY_FOLDER="KEYS"

mkdir ${KEY_FOLDER} 

#clone git from server
git clone server-gitolite:gitolite-admin


#### the keypair for the verificatore user are fixed (not created at any system delivery)
# (if required create them by issuing)
# ssh-keygen -t rsa -b2048 -N "" -f verificatore

cd gitolite-admin
cp ../verificatore.pub ./keydir
git add keydir
cd ..

# Elaborate students: 1 repo, 1 key per student
while read i; do
		echo $i;
		
		#tokenize each line
		token=($i);

		#prepare key pair w/o password
		if [[ ! -z  $i  ]]; then
			echo -e "\n###########\nKeygen for " ${token[0]} " " ${token[1]}  
			key=${token};
			ssh-keygen -t rsa -b2048 -N "" -f $key
		fi
		#move keys
		mv ${key}* ./${KEY_FOLDER}/


		# send pubkey via email
		mutt -s "Progetto Finale Reti Logiche (Prof. XXX)" ${token[1]} -e "my_hdr From:verificatore <verificatore@polimi.it>" -i ./email_message.txt -a ./${KEY_FOLDER}/${token[0]} < /dev/null  


		# set key and new repo @gitolite server
		cd gitolite-admin
		## add user to gitolite
		cp ../${KEY_FOLDER}/${token[0]}.pub ./keydir
		git add keydir/
		
		## add repo for user
		echo -ne "\nrepo ${token[0]}\n" >> ./conf/gitolite.conf
		echo -ne "\t RW+ = ${token[0]} master verificatore\n\n" >> ./conf/gitolite.conf
		git add ./conf/gitolite.conf

		## push per user
		git commit -m "added user ${token[0]}, ${token[1]} and corresponding repo"
		git push

		# back to the main folder
		cd ..
		
done < ${NAME_FILE}
