############################################################################
 #   Copyright (C) 2018 Davide Zoni                                       # 
 #                                                                        # 
 #   This program is free software; you can redistribute it and/or modify # 
 #   it under the terms of the GNU General Public License as published by # 
 #   the Free Software Foundation; either version 2 of the License, or    # 
 #   (at your option) any later version.                                  # 
 #                                                                        # 
 #   This program is distributed in the hope that it will be useful,      # 
 #   but WITHOUT ANY WARRANTY; without even the implied warranty of       # 
 #   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
 #   GNU General Public License for more details.                         # 
 #                                                                        # 
 #   You should have received a copy of the GNU General Public License    # 
 #   along with this program; if not, see <http://www.gnu.org/licenses/>  # 
############################################################################
 

#!/bin/bash
NAME_FILE="../studenti.txt"
KEY_FOLDER="../KEYS"

# DESCRIPTION:
# The script is only used to resend the already created privatekey to all the
# students enrolled in "Progetto finale di reti logiche". The keys are taken
# from the KEY folder and NOTHING is update on the gitolite-admin repo.

echo -e "\n##### INFO: START sending again the private key to all the students###\n";
while read i; do
		#echo $i;
		
		#tokenize each line
		token=($i);
		
		##for t in "${token[@]}"; do
		##		echo $t
		##done

		#prepare key pair w/o password
		if [[ ! -f  ./${KEY_FOLDER}/${token[0]}  ]]; then
			echo -e "ERROR: key for " ${token[0]} " " ${token[3]} " NOT FOUND"
		else
			echo -e "INFO: Sending key for " ${token[0]} " " ${token[3]} 
		fi
		
		# send pubkey via email
		mutt -s "Progetto Finale Reti Logiche (Prof. XXX)" ${token[3]} -e "my_hdr From:verificatore <verificatore@polimi.it>" -i ./email_message.txt -a ./${KEY_FOLDER}/${token[0]} < /dev/null  
		
done < ${NAME_FILE}

echo -e "\n##### INFO: END sending again the private key to all the students###\n";
